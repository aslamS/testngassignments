package com.ng;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "src/test/resources/Features/Test.feature", glue = "com.ng")
public class TestRunnertest extends AbstractTestNGCucumberTests {

}
